// Pseudo classical

document.writeln("Inheritance.js")


Function.method('new', function () {
    // Create a new object that inherits from the
    // constructor's prototype.
    var that = Object.create(this.prototype);
    // Invoke the constructor, binding –this- to
    // the new object.
    var other = this.apply(that, arguments);
    // If its return value isn't an object,
    // substitute the new object.
    return (typeof other === 'object' && other) || that;
});

//We can define a constructor and augment its prototype:
var Mammal = function (name) {
    this.name = name;
};
Mammal.prototype.get_name = function () {
    return this.name;
};

Mammal.prototype.says = function () {
    return this.saying || '';
}
//Now, we can make an instance:
var myMammal = new Mammal('Herb the Mammal');
var name = myMammal.get_name(); // 'Herb the Mammal'


// Object specifiers

// var myObject = maker(f, l, m, c, s);  // Instead of using too many parameters
// var myObject = maker({  // We can use this way to specify the construction of the object
//     first: f,
//     last: l,
//     middle: m,
//     state: s,
//     city: c
// });


// Prototypal

var myMammal = { // Let's start by creating a simple object
    name: 'Herb the Mammal', get_name: function () {
        return this.name;
    },
    says: function () {
        return this.saying || '';
    }
};

var myCat = Object.create(myMammal);
myCat.name = 'Henrietta';
myCat.saying = 'meow';
myCat.purr = function (n) {
    var i, s = '';
    for (i = 0; i < n; i += 1) {
        if (s) {
            s += '-';
        }
        s += 'r';
    }
    return s;
};
myCat.get_name = function () {
    return this.says() + ' ' + this.name + ' ' + this.says();
};
// This is differential inheritance. By customizing a new object, we specify the differences from the object on which it is based.


// Functional
// In Js we don't have private variables or methods

var mammal = function (spec) {
    var that = {};
    that.get_name = function () {
        return spec.name;
    };
    that.says = function () {
        return spec.saying || '';
    };
    return that; // We only return that
};
var myMammal = mammal({name: 'Herb'});

// Parts

// We can compose objects out of sets of parts.
var eventuality = function (that) {
    var registry = {};
    that.fire = function (event) {
        // Fire an event on an object. The event can be either
        // a string containing the name of the event or an
        // object containing a type property containing the
        // name of the event. Handlers registered by the 'on'
        // method that match the event name will be invoked.
        var array,
            func,
            handler,
            i,
            type = typeof event === 'string' ? event : event.type;
        // If an array of handlers exist for this event, then
        // loop through it and execute the handlers in order.
        if (registry.hasOwnProperty(type)) {
            array = registry[type];
            for (i = 0; i < array.length; i += 1) {
                handler = array[i];
                // A handler record contains a method and an optional
                // array of parameters. If the method is a name, look
                // up the function.
                func = handler.method;
                if (typeof func === 'string') {
                    func = this[func];
                }
                // Invoke a handler. If the record contained
                // parameters, then pass them. Otherwise, pass the
                // event object.
                func.apply(this,
                    handler.parameters || [event]);
            }
        }
        return this;
    };
    that.on = function (type, method, parameters) {
        // Register an event. Make a handler record. Put it
        // in a handler array, making one if it doesn't yet
        // exist for this type.
        var handler = {
            method: method,
            parameters: parameters
        };
        if (registry.hasOwnProperty(type)) {
            registry[type].push(handler);
        } else {
            registry[type] = [handler];
        }
        return this;
    };
    return that;
};