// Regular expressions

var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
var url = "http://www.ora.com:80/goodparts?q#fragment";
var result = parse_url.exec(url);
var names = ['url', 'scheme', 'slash', 'host', 'port',
    'path', 'query', 'hash'];
var blanks = '       ';
var i;
for (i = 0; i < names.length; i += 1) {
    document.writeln(names[i] + ':' +
        blanks.substring(names[i].length), result[i]);
}
/*
This produces:
 url:    http://www.ora.com:80/goodparts?q#fragment
scheme: http
slash:  //
host:   www.ora.com
port:   80
path:   goodparts
query:  q
hash:   fragment

 */

// ? Optional, it means repeat 0 or 1 time
// ^ Start of a string
// (?:...) non capturing group,
// (...) capturing group
// [...] character class
// A-Za-z, contains 26 uppercase letters and 26 lower- case letter
// The hypens indicate ranges, from A to Z
// + as a suffix indicates that the character class will be matched one or more times.
// \/ indicates that a / (slash) character should me matched. \ (backslash) is the scape character
// The suffix {0,3} indicates that the / will be matched, 0, 1, 2 or 3 times
// ([0-9.\-A-Za-z]+) this group will match one or more digits, letters, or . or - (\- is escaped)
// (?::(\d+))? will check for a series of digits preceded by :
// [^?#] all characters except ? and #
// $ end of a string


//There are three flags that can be set on a RegExp.
//g Global (match multiple times; the precise meaning of this varies with the method)
// i Insensitive (ignore character case)
// m Multiline (^ and $ can match line-ending characters)

var my_regexp = new RegExp("'(?:\\\\.|[^\\\\\'])*'", 'g');

// \ / [] ( ){ } ? +*| . ^ $ controll characters and special characters
// They must be escaped with / to make it literal.