var empty_object = {};

var stooge = {
    "first-name": "Jerome",
    "last-name": "Howard"
}
// Objects can also be nested
var flight = {
    airline: "Oceanic",
    number: 815,
    departure: {
        IATA: "SYD",
        time: "2004-09-22 14:55",
        city: "Sydney"
    }, arrival: {
        IATA: "LAX",
        time: "2004-09-23 10:42",
        city: "Los Angeles"
    } };

//Retrieval
stooge["first-name"]     // "Jerome"
flight.departure.IATA    // "SYD"

stooge["middle-name"] // undefined
flight.status // undefined
stooge["FIRST-NAME"]  //undefined

//The || operator can be used to fill in default values:

var middle = stooge["middle-name"] || "(none)";
var status = flight.status || "unknown";
/*
flight.equipment // undefined
flight.equipment.model // throw "TypeError"
flight.equipment && flight.equipment.model // undefined
*/

//Update
// if prop exists, value is replaced if not its created
stooge['middle-name'] = 'Lester';
stooge.nickname = 'Curly';

// Reference
// Objects are passed around by reference!!

var x = stooge;
x.nickname = 'Curly';
var nick = stooge.nickname;
// nick is 'Curly' because x and stooge
// are references to the same object


// Prototype
// Every object it's linked into a prototype object fromn which it can inherit props.

if (typeof Object.create !== 'function') {
    Object.create = function (o) {
        var F = function () {};
        F.prototype = o;
        return new F();
    };
}
var another_stooge = Object.create(stooge);

another_stooge['first-name'] = 'Harry';
another_stooge['middle-name'] = 'Moses';
another_stooge.nickname = 'Moe';

stooge.profession = 'actor';
another_stooge.profession    // 'actor'


// Reflection

typeof flight.number // 'number'
typeof flight.status // 'string'
typeof flight.arrival // 'object'
typeof flight.manifest // 'undefined'

// The other method is using:
flight.hasOwnProperty('number')         // true
flight.hasOwnProperty('constructor')    // false

// Enumeration
var name;
for (name in another_stooge) { // There is no guarantee on the order of names
    if (typeof another_stooge[name] !== 'function') {
        document.writeln(name + ': ' + another_stooge[name]);
    } }

// Delete
another_stooge.nickname    // 'Moe'
delete another_stooge.nickname;
another_stooge.nickname    // 'Curly'


// Global Abatement
//One way to minimize the use of global variables is to create a single global variable for your application:
var MYAPP = {};
MYAPP.stooge = {
    "first-name": "Joe",
    "last-name": "Howard"
};
MYAPP.flight = {
    airline: "Oceanic",
    number: 815,
    departure: {
        IATA: "SYD",
        time: "2004-09-22 14:55",
        city: "Sydney"
    }, arrival: {
        IATA: "LAX",
        time: "2004-09-23 10:42",
        city: "Los Angeles"
    } };
