document.writeln('Using Arrays');

var empty = [];
var numbers = [
    'zero', 'one', 'two', 'three', 'four',
    'five', 'six', 'seven', 'eight', 'nine'
];
empty[1] // undefined
numbers[1] // 'one'
empty.length // 0
numbers.length // 10

// The object literal:
var numbers_object = {
    '0': 'zero', '1': 'one', '2': 'two',
    '3': 'three', '4': 'four', '5': 'five',
    '6': 'six', '7': 'seven', '8': 'eight',
    '9': 'nine'
};

// In most languages, the elements of an array are all required to be of the same type. JavaScript allows an array to contain any mixture of values:
var misc = [
    'string', 98.6, true, false, null, undefined,
    ['nested', 'array'], {object: true}, NaN,
    Infinity
];
misc.length    // 10

// Length

var myArray = [];
myArray.length             // 0
myArray[1000000] = true;
myArray.length             // 1000001
// myArray contains one property.

numbers[numbers.length] = 'shi';
// numbers is ['zero', 'one', 'two', 'shi']

numbers.push('go');
// numbers is ['zero', 'one', 'two', 'shi', 'go']

// Delete
// Since JavaScript’s arrays are really objects, the delete operator can be used to remove elements from an array:
delete numbers[2];
// numbers is ['zero', 'one', undefined, 'shi', 'go']
// check the undefined!!
// We use splice instead
numbers.splice(2, 1);
// numbers is ['zero', 'one', 'shi', 'go']


// Enumeration
var i;
for (i = 0; i < myArray.length; i += 1) {
    document.writeln(myArray[i]);
}

// JavaScript does not have a good mechanism for distinguishing between arrays and objects. We can work around that deficiency by defining our own is_array function:
var is_array = function (value) {
    return value && typeof value === 'object' && value.constructor === Array;
};

// Array methods
// Suppose we want to add an array method that will allow us to do com- putation on an array:
Array.method('reduce', function (f, value) {
    var i;
    for (i = 0; i < this.length; i += 1) {
        value = f(this[i], value);
    }
    return value;
});

// Create an array of numbers.
var data = [4, 8, 15, 16, 23, 42];
// Define two simple functions. One will add two
// numbers. The other will multiply two numbers.
var add = function (a, b) {
    return a + b;
};
var mult = function (a, b) {
    return a * b;
};
// Invoke the data's reduce method, passing in the
// add function.
var sum = data.reduce(add, 0);    // sum is 108
// Invoke the reduce method again, this time passing
// in the multiply function.
var product = data.reduce(mult, 1);
// product is 7418880

// Give the data array a total function.
data.total = function () {
    return this.reduce(add, 0); // Arrays are also objects in JS
};
total = data.total(); // total is 108; this doesn't change data.length


// Dimensions
// JavaScript does not have arrays of more than one dimension, but like most C lan- guages, it can have arrays of arrays:
var matrix = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]];

matrix[2][1] // 7
