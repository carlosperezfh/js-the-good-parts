// Array methods

var a = ['a', 'b', 'c'];
var b = ['x', 'y', 'z'];
var c = a.concat(b, true); // c is ['a', 'b', 'c', 'x', 'y', 'z', true]


var a = ['a', 'b', 'c'];
a.push('d');
var c = a.join('');    // c is 'abcd';

var a = ['a', 'b', 'c'];
var c = a.pop(); // a is ['a', 'b'] & c is 'c'

var a = ['a', 'b', 'c'];
var b = ['x', 'y', 'z'];
var c = a.push(b, true);
// a is ['a', 'b', 'c', ['x', 'y', 'z'], true]
// c is 5;

var a = ['a', 'b', 'c'];
var b = a.reverse();
// both a and b are ['c', 'b', 'a']

var a = ['a', 'b', 'c'];
var c = a.shift(); // a is ['b', 'c'] & c is 'a'

var a = ['a', 'b', 'c'];
var b = a.slice(0, 1);
var c = a.slice(1);
var d = a.slice(1, 2);
// b is ['a']
// c is ['b', 'c']
// d is ['b']

var n = [4, 8, 15, 16, 23, 42]; n.sort( );
// n is [15, 16, 23, 4, 42, 8]

var a = ['a', 'b', 'c'];
var r = a.splice(1, 1, 'ache', 'bug');
// a is ['a', 'ache', 'bug', 'c']
// r is ['b']

var a = ['a', 'b', 'c'];
var r = a.unshift('?', '@');
// a is ['?', '@', 'a', 'b', 'c']
// r is 5 (array length)


// Number methods
document.writeln(Math.PI.toExponential( ));
document.writeln(Math.PI.toFixed(2));
document.writeln(Math.PI.toPrecision(2));
document.writeln(Math.PI.toString(2));

// Object

var a = {member: true};
var b = Object.create(a);
var t = a.hasOwnProperty('member');
var u = b.hasOwnProperty('member');
var v = b.member;


// String

var name = 'Curly';
var initial = name.charAt(0);    // initial is 'C'

var name = 'Curly';
var initial = name.charCodeAt(0);    // initial is 67

var s = 'C'.concat('a', 't'); // s is 'Cat'

var text = 'Mississippi';
var p = text.indexOf('ss');
p = text.indexOf('ss', 3);
p = text.indexOf('ss', 6);
// p is 2
// p is 5
// p is -1

var text = 'Mississippi';
var p = text.lastIndexOf('ss');
p = text.lastIndexOf('ss', 3);
p = text.lastIndexOf('ss', 6);
// p is 5
// p is 2
// p is 5

var m = ['AAA', 'A', 'aa', 'a', 'Aa', 'aaa'];
m.sort(function (a, b) {
    return a.localeCompare(b);
});
// m (in some locale) is
//      ['a', 'A', 'aa', 'Aa', 'aaa', 'AAA']

var oldareacode = /\((\d{3})\)/g;
var p = '(555)666-1212'.replace(oldareacode, '$1-');
// p is '555-666-1212'


var text = 'and in it he says "Any damn fool could';
var pos = text.search(/["']/);    // pos is 18

var text = 'and in it he says "Any damn fool could';
var a = text.slice(18);
// a is '"Any damn fool could'

var digits = '0123456789';
var a = digits.split('', 5);
// a is ['0', '1', '2', '3', '4']

