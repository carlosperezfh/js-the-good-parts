// Functions in JS are objects. Function objects are linked to the Function.prototype. Every fn is also created with the prototype property

// Function literal
// Create a variable called add and store a function
// in it that adds two numbers.
var add = function (a, b) {
    return a + b;
};

// Invocation
/* In addition to the declared params, every fn receives two additional params: this and arguments.
The value of this depends on the invocation pattern
4 types of invocations in JS:
    - Method invocation pattern
    - Function invocation pattern
    - Constructor invocation pattern
    - Apply invocation pattern
*/

// Method invocation pattern
// When a fn is stored as a property of an object, we call it method. When a method is invoked, this is bound to that object

var myObject = {
    value: 0,
    increment: function (inc) {
        this.value += typeof inc === 'number' ? inc : 1;
    }
};

myObject.increment();
document.writeln(myObject.value); // 1

myObject.increment(2);
document.writeln(myObject.value); // 3

// Function invocation pattern
// when a fn is not the property of an object, then its invoked as a function:
var sum = add(3, 5) // sum is 8
// When a fn is invoked like this, this is bound to the global object.

// Constructor invocation pattern
// JS is a prototypal inheritance language. Properties inherits properties directly from other objcs
// Create a constructor function called Quo.
// It makes an object with a status property.
var Quo = function (string) {
    this.status = string; // this is the new object
};
// Give all instances of Quo a public method
// called get_status.
Quo.prototype.get_status = function () {
    return this.status;
};
// Make an instance of Quo.
var myQuo = new Quo("confused");
document.writeln(myQuo.get_status()); // confused

// Apply invocation pattern
//JS is a functional based language, functions can have methods
// The apply method let us construct an array of arguments to use to invoke a function. It also let us choose the value of thos.
//

var array = [3, 4];
var sum = add.apply(null, array); // null is the value of this; sum is 7.

var statusObject = {
    status: 'A-OK'
};
// statusObject does not inherit from Quo.prototype,
// but we can invoke the get_status method on
// statusObject even though statusObject does not have
// a get_status method.
var status = Quo.prototype.get_status.apply(statusObject); // status is 'A-OK'

/////////////////////////////////////////////////////////////////////////////////

// Arguments

//A bonus parameter that is available to functions when they are invoked is the arguments array
var sum = function () {
    var i, sum = 0;
    for (i = 0; i < arguments.length; i += 1) {
        sum += arguments[i];
    }
    return sum;
};
document.writeln(sum(4, 8, 15, 16, 23, 42)); // 108

// Return

// Causes a fn to exit early
// If the fn was invoked with the new prefix and the return value is not an object, then this (the new object) is returned instead.


// Exceptions

// JS provides exception handling mechanism

var add = function (a, b) {
    if (typeof a !== 'number' || typeof b !== 'number') {
        throw {
            name: 'TypeError',
            message: 'add needs numbers'
        };
    }
    return a + b;
}

// Make a try_it function that calls the new add
// function incorrectly.
var try_it = function () {
    try {
        add("seven");
    } catch (e) {
        document.writeln(e.name + ': ' + e.message);
    }
}
try_it();


// Augmenting Types

// JS allow the basic types of the language to be augmented.
// (extensions in Swift?)

Function.prototype.method = function (name, func) {
    this.prototype[name] = func;
    return this;
};
//By augmenting Function.prototype with a method method, we no longer have to type
// the name of the prototype property. That bit of ugliness can now be hidden.

Number.method('integer', function () {
    return Math[this < 0 ? 'ceil' : 'floor'](this);
});
document.writeln((-10 / 3).integer()); // -3

/// JavaScript lacks a method that removes spaces from the ends of a string. That is an easy oversight to fix:
String.method('trim', function () {
    return this.replace(/^\s+|\s+$/g, '');
});
document.writeln('"' + " neat ".trim() + '"');

// Recursion

// A function that calls itself

// Define a walk_the_DOM function that visits every
// node of the tree in HTML source order, starting
// from some given node. It invokes a function,
// passing it each node in turn. walk_the_DOM calls
// itself to process each of the child nodes.
var walk_the_DOM = function walk(node, func) {
    func(node);
    node = node.firstChild;
    while (node) {
        walk(node, func);
        node = node.nextSibling;
    }
};

// Scope
// Scope in a programming language controls the visibility and lifetimes of variables
// and parameters.

var foo = function () {
    var a = 3, b = 5;
    var bar = function () {
        var b = 7, c = 11;
        // At this point, a is 3, b is 7, and c is 11
        a += b + c;
        // At this point, a is 21, b is 7, and c is 11
    };
    // At this point, a is 3, b is 5, and c is not defined
    bar();
    // At this point, a is 21, b is 5
};


// Closure

var myObject = (function () {
    var value = 0;
    return {
        increment: function (inc) {
            value += typeof inc === 'number' ? inc : 1;
        },
        getValue: function () {
            return value;
        }
    };
}());

//We are not assigning a function to myObject.
// We are assigning the result of invoking that function.
// Notice the ( ) on the last line. The function returns an object containing two methods,
// and those methods continue to enjoy the privilege of access to the value variable.


// Callbacks

//An approach to make an asynchronous request
/*
    request = prepare_the_request();
    send_request_asynchronously(request, function (response) {
        display(response);
    });
 */


// Module

// A module is a fn or object that represents an interface but it hides its state and implementation


// Memoization

/// A function that stores the result from previous operations, making it possible to avoid unnecesary work

var memoizer = function (memo, formula) {
    var recur = function (n) {
        var result = memo[n];
        if (typeof result !== 'number') {
            result = formula(recur, n);
            memo[n] = result;
        }
        return result;
    };
    return recur;
};

var fibonacci = memoizer([0, 1], function (recur, n) {
    return recur(n - 1) + recur(n - 2);
});

var factorial = memoizer([1, 1], function (recur, n) {
    return n * recur(n - 1);
});